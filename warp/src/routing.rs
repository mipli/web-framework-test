#![warn(rust_2018_idioms)]

use crate::error_handling::WarpError;
use fehler::*;
use serde::Deserialize;
use sqlx::PgPool;
use warp::{reject, Rejection};

#[derive(Debug, Deserialize)]
pub struct CreateUser {
    name: String,
}

#[derive(Debug, Deserialize)]
pub struct CreateBusiness {
    name: String,
}

#[derive(Debug, Deserialize)]
pub struct CreateTransaction {
    user_id: i32,
    business_id: i32,
    value: i32,
    currency: String,
}

#[derive(Debug)]
pub struct User {
    name: String,
}

pub struct State {
    pool: PgPool,
    user: Option<User>,
}

#[throws(Rejection)]
fn authorize(state: &State) -> () {
    match &state.user {
        Some(user) if user.name == "super" => (),
        _ => throw!(reject::custom(WarpError::Unauthorized)),
    }
}

pub mod filters {
    use super::handlers;
    use super::{CreateBusiness, CreateTransaction, CreateUser, State, User};
    use sqlx::PgPool;
    use warp::Filter;

    pub fn with_state(
        pool: PgPool,
    ) -> impl Filter<Extract = (State,), Error = warp::Rejection> + Clone {
        warp::any()
            .and(warp::header::optional::<String>("Authorization"))
            .map(|authorization| match authorization {
                Some(name) => Some(User { name }),
                None => None,
            })
            .map(move |user| State {
                user,
                pool: pool.clone(),
            })
            .boxed()
    }

    pub fn all(
        pool: PgPool,
    ) -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
        get_user(pool.clone())
            .or(get_users(pool.clone()))
            .or(create_user(pool.clone()))
            .or(get_business(pool.clone()))
            .or(get_businesses(pool.clone()))
            .or(create_business(pool.clone()))
            .or(get_transaction(pool.clone()))
            .or(create_transaction(pool.clone()))
            .or(get_transactions(pool))
    }

    pub fn get_user(
        pool: PgPool,
    ) -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
        warp::path!("user" / i32)
            .and(warp::get())
            .and(with_state(pool))
            .and_then(handlers::get_user)
    }

    pub fn get_users(
        pool: PgPool,
    ) -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
        warp::path!("users")
            .and(warp::get())
            .and(with_state(pool))
            .and_then(handlers::get_users)
    }

    pub fn create_user(
        pool: PgPool,
    ) -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
        warp::path!("user")
            .and(warp::post())
            .and(warp::body::content_length_limit(1024 * 16))
            .and(warp::body::json::<CreateUser>())
            .and(with_state(pool))
            .and_then(handlers::create_user)
    }

    pub fn get_business(
        pool: PgPool,
    ) -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
        warp::path!("business" / i32)
            .and(warp::get())
            .and(with_state(pool))
            .and_then(handlers::get_business)
    }

    pub fn get_businesses(
        pool: PgPool,
    ) -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
        warp::path!("businesses")
            .and(warp::get())
            .and(with_state(pool))
            .and_then(handlers::get_businesses)
    }

    pub fn create_business(
        pool: PgPool,
    ) -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
        warp::path!("business")
            .and(warp::post())
            .and(warp::body::content_length_limit(1024 * 16))
            .and(warp::body::json::<CreateBusiness>())
            .and(with_state(pool))
            .and_then(handlers::create_business)
    }

    pub fn get_transaction(
        pool: PgPool,
    ) -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
        warp::path!("transaction" / i64)
            .and(warp::get())
            .and(with_state(pool))
            .and_then(handlers::get_transaction)
    }

    pub fn get_transactions(
        pool: PgPool,
    ) -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
        warp::path!("transactions")
            .and(warp::get())
            .and(with_state(pool))
            .and_then(handlers::get_transactions)
    }

    pub fn create_transaction(
        pool: PgPool,
    ) -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
        warp::path!("transaction")
            .and(warp::post())
            .and(warp::body::content_length_limit(1024 * 16))
            .and(warp::body::json::<CreateTransaction>())
            .and(with_state(pool))
            .and_then(handlers::create_transaction)
    }
}

pub mod handlers {
    use super::{authorize, CreateBusiness, CreateTransaction, CreateUser, State};
    use crate::error_handling::WarpError;
    use base::{Business, Transaction, User};
    use fehler::*;
    use warp::{reject, Rejection};

    #[throws(Rejection)]
    pub async fn get_user(id: i32, state: State) -> impl warp::Reply {
        let user = User::get(id, &state.pool).await.or_else(|err| {
            let error: WarpError = err.into();
            Err(reject::custom(error))
        })?;

        warp::reply::json(&user)
    }

    #[throws(Rejection)]
    pub async fn get_users(state: State) -> impl warp::Reply {
        let users = User::get_all(&state.pool).await.or_else(|err| {
            let error: WarpError = err.into();
            Err(reject::custom(error))
        })?;
        warp::reply::json(&users)
    }

    #[throws(Rejection)]
    pub async fn create_user(user: CreateUser, state: State) -> impl warp::Reply {
        authorize(&state)?;
        let user = User::create(&user.name, &state.pool).await.or_else(|err| {
            let error: WarpError = err.into();
            Err(reject::custom(error))
        })?;
        warp::reply::json(&user)
    }

    #[throws(Rejection)]
    pub async fn get_business(id: i32, state: State) -> impl warp::Reply {
        let business = Business::get(id, &state.pool).await.or_else(|err| {
            let error: WarpError = err.into();
            Err(reject::custom(error))
        })?;

        warp::reply::json(&business)
    }

    #[throws(Rejection)]
    pub async fn get_businesses(state: State) -> impl warp::Reply {
        let businesses = Business::get_all(&state.pool).await.or_else(|err| {
            let error: WarpError = err.into();
            Err(reject::custom(error))
        })?;

        warp::reply::json(&businesses)
    }

    #[throws(Rejection)]
    pub async fn create_business(business: CreateBusiness, state: State) -> impl warp::Reply {
        authorize(&state)?;
        let business = Business::create(&business.name, &state.pool)
            .await
            .or_else(|err| {
                let error: crate::error_handling::WarpError = err.into();
                Err(reject::custom(error))
            })?;

        warp::reply::json(&business)
    }

    #[throws(Rejection)]
    pub async fn get_transaction(id: i64, state: State) -> impl warp::Reply {
        let transaction = Transaction::get(id, &state.pool).await.or_else(|err| {
            let error: WarpError = err.into();
            Err(reject::custom(error))
        })?;

        warp::reply::json(&transaction)
    }

    #[throws(Rejection)]
    pub async fn get_transactions(state: State) -> impl warp::Reply {
        let transactions = Transaction::get_all(&state.pool).await.or_else(|err| {
            let error: WarpError = err.into();
            Err(reject::custom(error))
        })?;

        warp::reply::json(&transactions)
    }

    #[throws(Rejection)]
    pub async fn create_transaction(
        transaction: CreateTransaction,
        state: State,
    ) -> impl warp::Reply {
        authorize(&state)?;
        let transaction = Transaction::create(
            transaction.user_id,
            transaction.business_id,
            transaction.value,
            &transaction.currency,
            &state.pool,
        )
        .await
        .or_else(|err| {
            let error: WarpError = err.into();
            Err(reject::custom(error))
        })?;

        warp::reply::json(&transaction)
    }
}
