#![warn(rust_2018_idioms)]

use anyhow::Error;
use dotenv::dotenv;
use fehler::*;
use sqlx::PgPool;
use std::env;
use warp::Filter;

mod error_handling;
mod routing;

pub struct Config {
    pool: PgPool,
}

#[throws(_)]
async fn init() -> Config {
    dotenv().ok();
    pretty_env_logger::init();

    let pool = PgPool::builder()
        .max_size(5)
        .build(&env::var("DATABASE_URL")?)
        .await?;

    Config { pool }
}

#[tokio::main]
async fn main() {
    let config = init().await.expect("Could not initialize program");

    let api = routing::filters::all(config.pool.clone()).recover(error_handling::handle_rejection);
    warp::serve(api).run(([127, 0, 0, 1], 3030)).await;
}
