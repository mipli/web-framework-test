#![warn(rust_2018_idioms)]

use base::error::BaseError;
use log::error;
use serde::Serialize;
use std::convert::Infallible;
use warp::{http::StatusCode, reject, Rejection, Reply};

/// An API error serializable to JSON.
#[derive(Serialize)]
struct ErrorMessage {
    #[serde(skip)]
    status: StatusCode,
    code: u16,
    message: String,
}

pub async fn handle_rejection(err: Rejection) -> Result<impl Reply, Infallible> {
    let error = if err.is_not_found() {
        ErrorMessage {
            status: StatusCode::NOT_FOUND,
            code: StatusCode::NOT_FOUND.as_u16(),
            message: "Page not found".to_string(),
        }
    } else if err.find::<warp::reject::MethodNotAllowed>().is_some() {
        ErrorMessage {
            status: StatusCode::METHOD_NOT_ALLOWED,
            code: StatusCode::METHOD_NOT_ALLOWED.as_u16(),
            message: "Method not allowed".to_string(),
        }
    } else if let Some(cause) = err.find::<warp::filters::body::BodyDeserializeError>() {
        ErrorMessage {
            status: StatusCode::BAD_REQUEST,
            code: StatusCode::BAD_REQUEST.as_u16(),
            message: format!("{}", cause),
        }
    } else if let Some(err) = err.find::<WarpError>() {
        error!("Warp Error: {:?}", err);
        match err {
            WarpError::Base(err) => ErrorMessage {
                status: StatusCode::BAD_REQUEST,
                code: StatusCode::BAD_REQUEST.as_u16(),
                message: format!("{:?}", err),
            },
            WarpError::Unauthorized => ErrorMessage {
                status: StatusCode::UNAUTHORIZED,
                code: StatusCode::UNAUTHORIZED.as_u16(),
                message: format!("{:?}", err),
            },
        }
    } else {
        // We should have expected this... Just log and say its a 500
        error!("Unhandled rejection: {:?}", err);
        ErrorMessage {
            status: StatusCode::INTERNAL_SERVER_ERROR,
            code: StatusCode::INTERNAL_SERVER_ERROR.as_u16(),
            message: "Unhandled rejection".to_string(),
        }
    };

    let json = warp::reply::json(&error);

    Ok(warp::reply::with_status(json, error.status))
}

#[derive(Debug)]
pub enum WarpError {
    Base(BaseError),
    Unauthorized,
}

impl reject::Reject for WarpError {}

impl From<BaseError> for WarpError {
    fn from(e: BaseError) -> Self {
        WarpError::Base(e)
    }
}
