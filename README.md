# Web Framework testing

Just a simple web application to test how some Rust web frameworks work

## Actix
- [x] Setup and state passing
- [x] Routing
- [ ] Request logging
- [ ] Error handling
- [ ] Authentication
- [ ] Authorization

### Notes
- feels like middleware setup is overly complicated, so I skipped that for now

## Warp
- [x] Setup and state passing
- [x] Routing
- [x] Error handling
- [ ] Request logging
- [x] Authentication
- [x] Authorization

### Notes
- very slow compile time, but I think boxing filters can help with this

## Tide
- [x] Setup and state passing
- [x] Routing
- [x] Error handling
- [ ] Request logging
- [x] Authentication
- [x] Authorization

### Notes
- Route handling is not optimal, but they're working on it and it can be solved for now using macros
