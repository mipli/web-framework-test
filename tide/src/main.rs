#![warn(rust_2018_idioms)]
use anyhow::Error;
use dotenv::dotenv;
use fehler::*;
use sqlx::PgPool;
use std::env;

mod authenticate;
mod error;
mod jwt;
mod routes;

#[derive(Clone)]
pub struct State {
    pool: PgPool,
}

#[throws(_)]
async fn init() -> State {
    dotenv().ok();
    pretty_env_logger::init();

    let pool = PgPool::builder()
        .max_size(5)
        .build(&env::var("DATABASE_URL")?)
        .await?;

    State { pool }
}

#[async_std::main]
async fn main() -> Result<(), std::io::Error> {
    let state = init().await.expect("Coult not initialize program");

    let mut app = tide::with_state(state);
    app = routes::add_routes(app);
    app.middleware(jwt::AuthenticationMiddleware::new());
    app.listen("127.0.0.1:3030").await?;
    Ok(())
}
