#![warn(rust_2018_idioms)]
use base::error::BaseError;
use serde::Serialize;
use tide::{Response, StatusCode};

/// An API error serializable to JSON.
#[derive(Serialize)]
struct ErrorMessage {
    #[serde(skip)]
    status: StatusCode,
    code: u16,
    message: String,
}

pub struct ErrorResponse(pub Response);

impl From<ErrorMessage> for ErrorResponse {
    fn from(em: ErrorMessage) -> Self {
        ErrorResponse(
            Response::new(em.status)
                .body_json(&em)
                .expect("Error parsing error output"),
        )
    }
}

impl ErrorResponse {
    pub fn unauthorized(message: String) -> Self {
        ErrorMessage {
            status: StatusCode::Unauthorized,
            code: StatusCode::Unauthorized as u16,
            message,
        }
        .into()
    }
}

impl From<Response> for ErrorResponse {
    fn from(e: Response) -> Self {
        Self(e)
    }
}

impl From<BaseError> for ErrorResponse {
    fn from(e: BaseError) -> ErrorResponse {
        match e {
            BaseError::UniqueViolation(message) | BaseError::ModelViolation(message) => {
                ErrorMessage {
                    status: StatusCode::Conflict,
                    code: StatusCode::Conflict as u16,
                    message,
                }
                .into()
            }
            _ => ErrorMessage {
                status: StatusCode::BadRequest,
                code: StatusCode::BadRequest as u16,
                message: format!("{:?}", e),
            }
            .into(),
        }
    }
}

impl From<serde_json::Error> for ErrorResponse {
    fn from(e: serde_json::Error) -> ErrorResponse {
        ErrorMessage {
            status: StatusCode::InternalServerError,
            code: StatusCode::InternalServerError as u16,
            message: format!("{:?}", e),
        }
        .into()
    }
}

impl From<std::num::ParseIntError> for ErrorResponse {
    fn from(e: std::num::ParseIntError) -> ErrorResponse {
        ErrorMessage {
            status: StatusCode::BadRequest,
            code: StatusCode::BadRequest as u16,
            message: format!("{:?}", e),
        }
        .into()
    }
}

impl From<std::io::Error> for ErrorResponse {
    fn from(e: std::io::Error) -> ErrorResponse {
        ErrorMessage {
            status: StatusCode::BadRequest,
            code: StatusCode::BadRequest as u16,
            message: format!("{:?}", e),
        }
        .into()
    }
}

impl From<ErrorResponse> for Response {
    fn from(e: ErrorResponse) -> Self {
        e.0
    }
}
