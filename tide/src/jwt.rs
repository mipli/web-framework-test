#![warn(rust_2018_idioms)]
use crate::{error::ErrorResponse, State};
use fehler::*;
use jsonwebtoken::errors::ErrorKind;
use jsonwebtoken::{decode, encode, Algorithm, DecodingKey, EncodingKey, Header, Validation};
use log::info;
use serde::{Deserialize, Serialize};
use std::future::Future;
use std::pin::Pin;
use tide::{Middleware, Next, Request};

pub(crate) type BoxFuture<'a, T> = Pin<Box<dyn Future<Output = T> + Send + 'a>>;

#[derive(Debug, Serialize, Deserialize)]
pub struct Claims {
    pub email: String,
    pub privilegies: String,
    pub exp: usize,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Account {
    pub email: String,
    pub privilegies: String,
}

impl From<&Claims> for Account {
    fn from(claims: &Claims) -> Self {
        Account {
            email: claims.email.to_string(),
            privilegies: claims.privilegies.to_string(),
        }
    }
}

#[derive(Clone, Default, Debug)]
pub struct AuthenticationMiddleware {}

impl AuthenticationMiddleware {
    pub fn new() -> Self {
        Self {}
    }

    async fn validate_jwt<'a, State: Send + Sync + 'static>(
        &'a self,
        ctx: Request<State>,
        next: Next<'a, State>,
    ) -> tide::Result {
        match ctx.header(&"Authorization".parse().unwrap()) {
            Some(h) if !h.is_empty() => {
                match h[0].as_str().split(' ').collect::<Vec<_>>().as_slice() {
                    [token, value] => {
                        let claims = decode::<Claims>(
                            value,
                            &DecodingKey::from_secret(b"jwtkey"),
                            &Validation::new(Algorithm::HS512),
                        )
                        .expect("Claims extraction failed");
                        let account = Account::from(&claims.claims);
                        info!("Account: {:#?}", account);
                        next.run(ctx.set_local(account)).await
                    }
                    _ => next.run(ctx).await,
                }
            }
            _ => next.run(ctx).await,
        }
    }
}

impl<State: Send + Sync + 'static> Middleware<State> for AuthenticationMiddleware {
    fn handle<'a>(
        &'a self,
        ctx: Request<State>,
        next: Next<'a, State>,
    ) -> BoxFuture<'a, tide::Result> {
        Box::pin(async move { self.validate_jwt(ctx, next).await })
    }
}

pub trait AuthenticationExt {
    fn get_account(&self) -> Option<&Account>;
}
impl<State> AuthenticationExt for Request<State> {
    fn get_account(&self) -> Option<&Account> {
        self.local::<Account>()
    }
}

pub fn create_claims(email: String, privilegies: String) -> String {
    let claims = Claims {
        email,
        privilegies,
        exp: 10000000000,
    };
    let key = b"jwtkey";

    let mut header = Header::default();
    header.kid = Some("signing_key".to_owned());
    header.alg = Algorithm::HS512;

    match encode(&header, &claims, &EncodingKey::from_secret(key)) {
        Ok(t) => t,
        Err(_) => panic!(), // in practice you would return the error
    }
}
