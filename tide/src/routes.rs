#![warn(rust_2018_idioms)]
use crate::{error::ErrorResponse, jwt::Account, jwt::AuthenticationExt, State};
use fehler::*;
use serde::{Deserialize, Serialize};
use tide::{Request, Response, Server, StatusCode};

macro_rules! GET {
    ($app:expr; $path:expr => $call:expr) => {
        $app.at($path)
            .get(|req| async move { $call(req).await.or_else(|r| Ok(r.0)) })
    };
}

macro_rules! POST {
    ($app:expr; $path:expr => $call:expr) => {
        $app.at($path)
            .post(|req| async move { $call(req).await.or_else(|r| Ok(r.0)) })
    };
}

#[derive(Debug, Deserialize)]
pub struct LoginRequest {
    email: String,
    privilegies: Option<String>,
}

#[derive(Debug, Deserialize)]
pub struct CreateUser {
    name: String,
}

#[derive(Debug, Deserialize)]
pub struct CreateBusiness {
    name: String,
}

#[derive(Debug, Deserialize)]
pub struct CreateTransaction {
    user_id: i32,
    business_id: i32,
    value: i32,
    currency: String,
}

// Routing ergonomics issue on Github: https://github.com/http-rs/tide/issues/452
// Orginal routing call:
//  - app.at("/users").get(|req| async move { get_users(req).await.or_else(|r| Ok(r.0)) });
pub fn add_routes(mut app: Server<State>) -> Server<State> {
    POST!(app; "/login" => login);

    POST!(app; "/user" => create_user);
    GET!(app; "/user/:id" => get_user);
    GET!(app; "/users" => get_users);

    POST!(app; "/transaction" => create_transaction);
    GET!(app; "/transaction/:id" => get_transaction);
    GET!(app; "/transactions" => get_transactions);

    POST!(app; "/business" => create_business);
    GET!(app; "/business/:id" => get_business);
    GET!(app; "/businesses" => get_businesses);

    app
}

#[derive(Debug, Serialize)]
pub struct Token {
    token: String,
}

#[throws(ErrorResponse)]
async fn login(mut req: Request<State>) -> Response {
    let payload: LoginRequest = req.body_json().await?;
    let token = Token {
        token: crate::jwt::create_claims(
            payload.email.to_string(),
            payload.privilegies.unwrap_or_else(|| "user".to_string()),
        ),
    };
    Response::new(StatusCode::Ok).body_json(&token)?
}

#[throws(ErrorResponse)]
async fn get_users(req: Request<State>) -> Response {
    crate::authenticate::authorize(&req, "user")?;
    let users = base::User::get_all(&req.state().pool).await?;
    Response::new(StatusCode::Ok).body_json(&users)?
}

#[throws(ErrorResponse)]
async fn get_user(req: Request<State>) -> Response {
    crate::authenticate::authorize(&req, "user")?;
    let id = req.param("id")?;
    let user = base::User::get(id, &req.state().pool).await?;
    Response::new(StatusCode::Ok).body_json(&user)?
}

#[throws(ErrorResponse)]
async fn create_user(mut req: Request<State>) -> Response {
    crate::authenticate::authorize(&req, "super")?;

    let user: CreateUser = req.body_json().await?;
    let user = base::User::create(&user.name, &req.state().pool).await?;
    Response::new(StatusCode::Ok).body_json(&user)?
}

#[throws(ErrorResponse)]
async fn get_transactions(req: Request<State>) -> Response {
    crate::authenticate::authorize(&req, "user")?;
    let transactions = base::Transaction::get_all(&req.state().pool).await?;
    Response::new(StatusCode::Ok).body_json(&transactions)?
}

#[throws(ErrorResponse)]
async fn get_transaction(req: Request<State>) -> Response {
    crate::authenticate::authorize(&req, "user")?;
    let id = req.param("id")?;
    let transaction = base::Transaction::get(id, &req.state().pool).await?;
    Response::new(StatusCode::Ok).body_json(&transaction)?
}

#[throws(ErrorResponse)]
async fn create_transaction(mut req: Request<State>) -> Response {
    crate::authenticate::authorize(&req, "super")?;

    let transaction: CreateTransaction = req.body_json().await?;
    let transaction = base::Transaction::create(
        transaction.user_id,
        transaction.business_id,
        transaction.value,
        &transaction.currency,
        &req.state().pool,
    )
    .await?;
    Response::new(StatusCode::Ok).body_json(&transaction)?
}

#[throws(ErrorResponse)]
async fn get_businesses(req: Request<State>) -> Response {
    crate::authenticate::authorize(&req, "user")?;
    let businesses = base::Business::get_all(&req.state().pool).await?;
    Response::new(StatusCode::Ok).body_json(&businesses)?
}

#[throws(ErrorResponse)]
async fn get_business(req: Request<State>) -> Response {
    crate::authenticate::authorize(&req, "user")?;
    let id = req.param("id")?;
    let business = base::Business::get(id, &req.state().pool).await?;
    Response::new(StatusCode::Ok).body_json(&business)?
}

#[throws(ErrorResponse)]
async fn create_business(mut req: Request<State>) -> Response {
    crate::authenticate::authorize(&req, "super")?;

    let business: CreateBusiness = req.body_json().await?;
    let business = base::Business::create(&business.name, &req.state().pool).await?;
    Response::new(StatusCode::Ok).body_json(&business)?
}
