#![warn(rust_2018_idioms)]

use crate::jwt::{Account, AuthenticationExt};
use crate::{error::ErrorResponse, State};
use fehler::*;
use log::debug;
use tide::{Middleware, Next, Request};

#[throws(ErrorResponse)]
pub fn authorize(req: &Request<State>, requirement: &str) -> () {
    let authorized = match req.get_account() {
        Some(account) => {
            debug!("account: {:?}", account);
            debug!("requirement: {:?}", requirement);
            account.privilegies == requirement
        }
        None => {
            debug!("no account found");
            false
        }
    };
    if !authorized {
        throw!(ErrorResponse::unauthorized(format!(
            "Unauthorized acess to: {}",
            req.uri()
        )))
    }
}
