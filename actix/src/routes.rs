#![warn(rust_2018_idioms)]
use actix_web::{http::StatusCode, web, HttpRequest, HttpResponse, Responder};
use base::{Business, Transaction, User};
use log::error;
use serde::{Deserialize, Serialize};
use sqlx::PgPool;

#[derive(Debug, Deserialize)]
pub struct CreateUser {
    name: String,
}

#[derive(Debug, Deserialize)]
pub struct CreateBusiness {
    name: String,
}

#[derive(Debug, Deserialize)]
pub struct CreateTransaction {
    user_id: i32,
    business_id: i32,
    value: i32,
    currency: String,
}

pub async fn get_businesses(pool: web::Data<PgPool>, _req: HttpRequest) -> impl Responder {
    let businesses = Business::get_all(&pool).await.unwrap();
    HttpResponse::Ok().json(businesses)
}

pub async fn get_business(
    pool: web::Data<PgPool>,
    _req: HttpRequest,
    path: web::Path<(i32,)>,
) -> impl Responder {
    let business = Business::get(path.0, &pool).await.unwrap();

    HttpResponse::Ok().json(business)
}

pub async fn create_business(
    business: web::Json<CreateBusiness>,
    pool: web::Data<PgPool>,
    _req: HttpRequest,
) -> impl Responder {
    let business = Business::create(&business.name, &pool).await.unwrap();

    HttpResponse::Ok().json(business)
}

pub async fn get_users(pool: web::Data<PgPool>, _req: HttpRequest) -> impl Responder {
    let users = User::get_all(&pool).await.unwrap();

    HttpResponse::Ok().json(users)
}

pub async fn get_user(
    pool: web::Data<PgPool>,
    _req: HttpRequest,
    path: web::Path<(i32,)>,
) -> impl Responder {
    let user = User::get(path.0, &pool).await.unwrap();

    HttpResponse::Ok().json(user)
}

pub async fn create_user(
    user: web::Json<CreateUser>,
    pool: web::Data<PgPool>,
    _req: HttpRequest,
) -> impl Responder {
    let user = User::create(&user.name, &pool).await.unwrap();

    HttpResponse::Ok().json(user)
}

pub async fn get_transactions(pool: web::Data<PgPool>, _req: HttpRequest) -> impl Responder {
    let transactions = Transaction::get_all(&pool).await.unwrap();

    HttpResponse::Ok().json(transactions)
}

pub async fn get_transaction(
    pool: web::Data<PgPool>,
    _req: HttpRequest,
    path: web::Path<(i64,)>,
) -> impl Responder {
    let transaction = Transaction::get(path.0, &pool).await.unwrap();

    HttpResponse::Ok().json(transaction)
}

pub async fn create_transaction(
    transaction: web::Json<CreateTransaction>,
    pool: web::Data<PgPool>,
    _req: HttpRequest,
) -> impl Responder {
    let transaction = Transaction::create(
        transaction.user_id,
        transaction.business_id,
        transaction.value,
        &transaction.currency,
        &pool,
    )
    .await;

    match transaction {
        Ok(transaction) => HttpResponse::Ok().json(transaction),
        Err(err) => {
            let message = format!("Database error: {:?}", err);
            error!("{}", message);
            HttpResponse::BadRequest().json(ErrorMessage {
                code: StatusCode::BAD_REQUEST.as_u16(),
                message,
            })
        }
    }
}

#[derive(Serialize)]
struct ErrorMessage {
    code: u16,
    message: String,
}
