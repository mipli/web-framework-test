#![warn(rust_2018_idioms)]
use actix_web::{
    error, http::StatusCode, middleware, web, App, FromRequest, HttpRequest, HttpResponse,
    HttpServer,
};
use anyhow::{anyhow, Result};
use dotenv::dotenv;
use serde::{Deserialize, Serialize};
use sqlx::PgPool;
use std::env;

mod routes;

/// An API error serializable to JSON.
#[derive(Serialize)]
struct ErrorMessage {
    code: u16,
    message: String,
}

#[derive(Deserialize)]
struct Dummy {}

fn json_error_handler(err: error::JsonPayloadError, _req: &HttpRequest) -> error::Error {
    use actix_web::error::JsonPayloadError;

    let detail = err.to_string();
    let resp = match &err {
        JsonPayloadError::ContentType => HttpResponse::UnsupportedMediaType().body(detail),
        JsonPayloadError::Deserialize(json_err) if json_err.is_data() => {
            HttpResponse::UnprocessableEntity().body(detail)
        }
        _ => HttpResponse::BadRequest().json(ErrorMessage {
            code: StatusCode::BAD_REQUEST.as_u16(),
            message: detail,
        }),
    };
    error::InternalError::from_response(err, resp).into()
}

#[actix_rt::main]
async fn main() -> Result<()> {
    pretty_env_logger::init();
    dotenv().ok();
    let pool = PgPool::builder()
        .max_size(5)
        .build(&env::var("DATABASE_URL").expect("Coult not read DATABASE_URL"))
        .await
        .or_else(|_| Err(anyhow!("Failed to open database connect")))?;

    let pool_data = web::Data::new(pool);

    HttpServer::new(move || {
        App::new()
            .app_data(pool_data.clone())
            .app_data(
                // change json extractor configuration
                web::Json::<Dummy>::configure(|cfg| cfg.error_handler(json_error_handler)),
            )
            .wrap(middleware::Logger::default())
            .route("/businesses", web::get().to(routes::get_businesses))
            .route("/business/{id}", web::get().to(routes::get_business))
            .route("/business", web::post().to(routes::create_business))
            .route("/users", web::get().to(routes::get_users))
            .route("/user/{id}", web::get().to(routes::get_user))
            .route("/user", web::post().to(routes::create_user))
            .route("/transactions", web::get().to(routes::get_transactions))
            .route("/transaction/{id}", web::get().to(routes::get_transaction))
            .route("/transaction", web::post().to(routes::create_transaction))
    })
    .bind("127.0.0.1:3030")?
    .run()
    .await
    .or_else(|_| Err(anyhow!("Failed to open database connect")))
}
