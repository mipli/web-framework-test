#![warn(rust_2018_idioms)]
use log::*;
use sqlx::error::DatabaseError;
use std::convert::TryFrom;

/// An error returned by a provider
#[derive(thiserror::Error, Debug)]
pub enum BaseError {
    /// The requested entity does not exist
    #[error("Entity does not exist")]
    NotFound,
    /// The operation violates a uniqueness constraint
    #[error("{0}")]
    UniqueViolation(String),
    /// The requested operation violates the data model
    #[error("{0}")]
    ModelViolation(String),
    #[error(transparent)]
    /// A generic unhandled error
    Provider(sqlx::Error),
}

impl From<sqlx::Error> for BaseError {
    fn from(e: sqlx::Error) -> Self {
        error!("sqlx returned err -- {:#?}", &e);
        match e {
            sqlx::Error::RowNotFound => BaseError::NotFound,
            sqlx::Error::Database(db_err) => {
                if let Some(pg_err) = db_err.try_downcast_ref::<sqlx::postgres::PgError>() {
                    if let Ok(provide_err) = BaseError::try_from(pg_err) {
                        return provide_err;
                    }
                }
                BaseError::Provider(sqlx::Error::Database(db_err))
            }
            _ => BaseError::Provider(e),
        }
    }
}

impl TryFrom<&sqlx::postgres::PgError> for BaseError {
    type Error = ();

    /// Postgres Error Codes: https://www.postgresql.org/docs/current/errcodes-appendix.html
    fn try_from(pg_err: &sqlx::postgres::PgError) -> Result<Self, Self::Error> {
        let provider_err = match pg_err.code().unwrap() {
            "23505" => BaseError::UniqueViolation(pg_err.details().unwrap().to_owned()),
            code if code.starts_with("23") => {
                BaseError::ModelViolation(pg_err.message().to_owned())
            }
            _ => return Err(()),
        };

        Ok(provider_err)
    }
}
