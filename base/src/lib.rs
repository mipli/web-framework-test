#![warn(rust_2018_idioms)]
use chrono::{DateTime, Utc};
use fehler::*;
use log::*;
use serde::{Deserialize, Serialize};
use serde_json::value::Value;
use sqlx::postgres::PgQueryAs;
use sqlx::PgPool;

pub mod error;
use error::BaseError;

#[derive(Debug, Deserialize, Serialize)]
pub struct User {
    pub id: i32,
    pub name: String,
    pub created_at: DateTime<Utc>,
}

impl User {
    #[throws(BaseError)]
    pub async fn get(id: i32, pool: &PgPool) -> Option<Self> {
        sqlx::query_as!(User, "SELECT * FROM users WHERE id = $1", id)
            .fetch_optional(pool)
            .await
            .or_else(|e| Err(BaseError::from(e)))?
    }

    #[throws(BaseError)]
    pub async fn get_all(pool: &PgPool) -> Vec<Self> {
        sqlx::query_as!(User, "SELECT * FROM users")
            .fetch_all(pool)
            .await
            .or_else(|e| Err(BaseError::from(e)))?
    }

    #[throws(BaseError)]
    pub async fn create(name: &str, pool: &PgPool) -> Self {
        sqlx::query_as!(
            User,
            "INSERT INTO users (name, created_at) VALUES ($1, now()) RETURNING *",
            name
        )
        .fetch_one(pool)
        .await
        .or_else(|e| Err(BaseError::from(e)))?
    }
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Business {
    pub id: i32,
    pub name: String,
    pub created_at: DateTime<Utc>,
}

impl Business {
    #[throws(BaseError)]
    pub async fn get(id: i32, pool: &PgPool) -> Option<Self> {
        sqlx::query_as!(Business, "SELECT * FROM businesses WHERE id = $1", id)
            .fetch_optional(pool)
            .await
            .or_else(|e| Err(BaseError::from(e)))?
    }

    #[throws(BaseError)]
    pub async fn get_all(pool: &PgPool) -> Vec<Self> {
        sqlx::query_as!(Business, "SELECT * FROM businesses")
            .fetch_all(pool)
            .await
            .or_else(|e| Err(BaseError::from(e)))?
    }

    #[throws(BaseError)]
    pub async fn create(name: &str, pool: &PgPool) -> Self {
        sqlx::query_as!(
            Business,
            "INSERT INTO businesses (name, created_at) VALUES ($1, now()) RETURNING *",
            name
        )
        .fetch_one(pool)
        .await
        .or_else(|e| Err(BaseError::from(e)))?
    }
}

#[derive(sqlx::Type, Debug, Deserialize, Serialize)]
pub enum TransactionStatus {
    Valid,
    Pending,
    Booked,
}

#[derive(sqlx::FromRow, Debug, Deserialize, Serialize)]
pub struct Transaction {
    pub id: i64,
    pub user_id: i32,
    pub business_id: i32,
    pub value: i32,
    pub currency: String,
    pub data: Option<Value>,
    pub status: TransactionStatus,
}

impl Transaction {
    #[throws(BaseError)]
    pub async fn get(id: i64, pool: &PgPool) -> Option<Self> {
        sqlx::query_as::<_, Transaction>("SELECT * FROM transactions WHERE id = $1")
            .bind(id)
            .fetch_optional(pool)
            .await
            .or_else(|e| Err(BaseError::from(e)))?
    }

    #[throws(BaseError)]
    pub async fn get_all(pool: &PgPool) -> Vec<Self> {
        sqlx::query_as::<_, Transaction>("SELECT * FROM transactions")
            .fetch_all(pool)
            .await
            .or_else(|e| Err(BaseError::from(e)))?
    }

    #[throws(BaseError)]
    pub async fn create(
        user_id: i32,
        business_id: i32,
        value: i32,
        currency: &str,
        pool: &PgPool,
    ) -> Self {
        sqlx::query_as::<_, Transaction>(
            "INSERT INTO transactions (user_id, business_id, value, currency, status) VALUES ($1, $2, $3, $4, 'Pending') RETURNING *")
            .bind(user_id)
            .bind(business_id)
            .bind(value)
            .bind(currency)
            .fetch_one(pool)
            .await
            .or_else(|e| Err(BaseError::from(e)))?
    }
}
